Using the uber_build.py build system for Android:
-------------------------------------------------


Requirements:

- Android SDK must be installed and ANDROID_HOME set.
- Java (java and javac executables) must be installed and available.
- Python must be installed and the executable 'python' must be available from the shell.


Usage:

1. Create a new Android application project:
    $ ./uber_build.py create_project myproject --target android-23
    or more specifically
    $ ./uber_build.py create_project myproject --name CarApplication --target android-23 --package com.uber.selfdriving --activity MainActivity

    Where:
    'myproject' is the directory where the new project will be created.
    'CarApplication' is the application name.
    'android-23' is the Android target API level.
    'com.uber.selfdriving' is the application package name.
    'MainActivity' is the name of the main/startup activity.

2. Compile an existing application project:
    From inside project directory:
    $ ./uber_build.py compile

    From any directory:
    $ ./uber_build.py compile <path-to-project>

3. Package an existing application project:
    From inside project directory:
    $ ./uber_build.py package

    From any directory:
    $ ./uber_build.py package <path-to-project>

4. Launch an existing application project:
    From inside project directory:
    $ ./uber_build.py launch

    From any directory:
    $ ./uber_build.py launch <path-to-project>

5. Clean an existing application project:
    From inside project directory:
    $ ./uber_build.py clean

    From any directory:
    $ ./uber_build.py clean <path-to-project>

6. To switch to different build tools, change API level, add/remove hook scripts see the uber_build.cfg inside the project directory.


Questions:

pesho.petrov@gmail.com

