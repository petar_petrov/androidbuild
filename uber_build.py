#!/usr/bin/env python

import argparse
import glob
import os
import re
import subprocess
import sys

#
# TODO:
# 1. Dependencies between targes. E.g. 'package' requires 'compile' and 'launch' requires 'package'.
# 2. Support Android targets with Google APIs.
# 3. Many more.
#

CMP_BEFORE = "compileBefore"
CMP_AFTER = "compileAfter"
PKG_BEFORE = "packageBefore"
PKG_AFTER = "packageAfter"
LNCH_BEFORE = "launchBefore"
LNCH_AFTER = "launchAfter"
CLEAN_BEFORE = "cleanBefore"
CLEAN_AFTER = "cleanAfter"

BUILD_TOOLS_PATH = "buildToolsPath"
PROJECT_NAME = "projectName"
API_LEVEL = "apiLevel"
CONFIG_FILE_NAME = "uber_build.cfg"
DEFAULT_CONFIG = """
# Changing this is not supported for now.
%(PROJECT_NAME)s = "_project_name_"

%(API_LEVEL)s = "_api_level_"
# Change this value to upgrade to newer build tools.
%(BUILD_TOOLS_PATH)s = "_build_tools_path_"

%(CMP_BEFORE)s = ["echo 'Test Compile Before'"]
%(CMP_AFTER)s = ["echo 'Test Compile After'"]

%(PKG_BEFORE)s = ["echo 'Test Package Before'"]
%(PKG_AFTER)s = ["echo 'Test Package After'"]

%(LNCH_BEFORE)s = ["echo 'Test Launch Before'"]
%(LNCH_AFTER)s = ["echo 'Test Launch After'"]

%(CLEAN_BEFORE)s = ["echo 'Test Clean Before'"]
%(CLEAN_AFTER)s = ["echo 'Test Clean After'"]

# TODO: Extra source directories, etc.
""" % (globals())

class UberBuild(object):
    def __init__(self, args):
        self.args = args
        self.config = {}
        self.srcDir = os.path.join(args.path, "src")
        self._javaVersion = '1.5'
        self._androidHome = os.environ.get('ANDROID_HOME')
        self._checkEnvironment()

    def _checkEnvironment(self):
        if 'ANDROID_HOME' not in os.environ:
            print "ANDROID_HOME is not set in your environment. Please set it and try again."
            sys.exit(1)

        # We should also check if "java" and "javac" are available.

        # Check if the Android SDK is sane.
        filePath = os.path.join(self._androidHome, "tools", "android")
        if not os.path.isfile(filePath):
            print "Could not find android executable in $ANDROID_HOME/tools/android (%s)" % (filePath)
            sys.exit(1)

    def _getConfigName(self):
        return os.path.join(self.args.path, CONFIG_FILE_NAME)

    def _loadConfig(self):
        content = open(self._getConfigName()).read()
        exec(content, globals(), self.config)
        self._buildToolsPath = self.config[BUILD_TOOLS_PATH]

    def _getBuildToolsPath(self):
        files = self._getAllFiles(os.path.join(self._androidHome, 'build-tools'), pattern=re.compile("^dx$"))
        #print "F: %s" % (files)
        if len(files) < 1:
            print "Could not find path to build tools"
            sys.exit(1)
        return files[-1][0: -3]

    def _saveConfig(self):
        configFile = open(self._getConfigName(), "w+")
        config = DEFAULT_CONFIG
        config = config.replace("_build_tools_path_", self._getBuildToolsPath())
        config = config.replace("_project_name_", self._getProjectName())
        config = config.replace("_api_level_", self._getApiLevelFromTarget(self.args.target))
        configFile.write(config)
        configFile.close()

    def _execCommand(self, cmd, shell=True):
        try:
             subprocess.check_call(cmd, shell=shell, stderr=subprocess.STDOUT)
             #subprocess.check_call(cmd, stderr=subprocess.STDOUT)
        except OSError, e:
             # Notify the users, so they can fix failing commends.
             print('Failed to execute \'%s\': %s' % (cmd, e))

    def _execCommands(self, cmds):
        for cmd in cmds:
            self._execCommand(cmd)

    def _runCmd(self, type):
        #print "Exec: [%s]" % (self.config)
        self._execCommands(self.config.get(type, []))

    def _getProjectName(self):
        if PROJECT_NAME in self.config:
            return self.config[PROJECT_NAME]
        return self.args.name or self.args.activity

    def _chooseTarget(self):
        print "List of valid Android targets:"
        args = [
            os.path.join(self._androidHome, 'tools', 'android'),
            'list',
            'targets'
        ]
        self._execCommand(args, shell=False)

    def _getApiLevelFromString(self, target):
        result = re.match("^android-(.+)$", target)
        if result is not None:
            return result.group(1)

        result = re.match("^.*:(\d+)$", target)
        if result is not None:
            return result.group(1)

        return None

    def _getApiLevelFromTarget(self, target):
        result = self._getApiLevelFromString(target)
        if result is None:
            # Otherwise it's an ID (1, 2, 3, etc). Check project.properties.
            f = open(self._getPath('project.properties'))
            while True:
                line = f.readline().strip()
                if line is None:
                    break
                if line.startswith("target="):
                    result = self._getApiLevelFromString(line[7:])
                    break

        if result:
            print "Android API level is %s" % (result)
            return result

        print "Could not determine Android API level from target. Please use android-API as --target."
        sys.exit(1)

    def createProject(self):
        if not self.args.target:
            print "Please use --target to specify a valid Android target API (e.g. android-23)."
            self._chooseTarget()
            return

        args = [
            os.path.join(self._androidHome, "tools", "android"),
            'create',
            'project',
            '--name',
            self._getProjectName(),
            '--path',
            self.args.path,
            '--package',
            self.args.package,
            '--target',
            self.args.target,
            '--activity',
            self.args.activity
        ]
        self._execCommand(args, shell=False)
        self._saveConfig()

        # Copy self to project folder.
        self._execCommand([
            'cp',
            os.path.realpath(__file__),
            self._getPath()
        ], shell=False)

    def _getAllFiles(self, folder, pattern=None):
        javaFiles = []
        for subdir, dirs, files in os.walk(folder):
            for file in files:
                if (pattern is None) or pattern.search(file):
                    javaFiles.append(os.path.join(subdir, file))
        return javaFiles

    def _getPath(self, *subPaths):
        return os.path.join(self.args.path, *subPaths)

    def _getApiLevel(self):
        return self.config[API_LEVEL]

    def _getAndroidJar(self):
        return os.path.join(self._androidHome, "platforms", "android-" + self._getApiLevel(), "android.jar")

    def _getBootstrapClassJar(self):
        # How about google APIs?
        jars = []
        jars.append(self._getAndroidJar())
        return ":".join(jars)

    def _mkDirNoError(self, path):
        try:
            os.mkdir(path)
        except OSError:
            pass

    def _compileJavaCode(self):
        print "Compiling ..."
        self._mkDirNoError(self._getPath('bin', 'classes'))
        args = ['javac']
        for fl in self._getAllFiles(self.srcDir):
            args.append(fl)
        args.extend([
            '-d',
            self._getPath('bin', 'classes'),
            '-classpath',
            self._getPath('bin', 'classes'),
            '-sourcepath',
            self.srcDir + ":" + self._getPath('gen'),
            '-target',
            self._javaVersion,
            '-bootclasspath',
            self._getBootstrapClassJar(),
            '-encoding',
            'UTF-8',
            '-source',
            self._javaVersion
        ])
        self._execCommand(args, shell=False)
        print "Done"

    def _dex(self):
        print "Dexing ..."
        args = [
            os.path.join(self._buildToolsPath, 'dx'),
            '--dex',
            '--output',
            self._getPath('bin', 'classes.dex'),
            self._getPath('bin', 'classes')
        ]
        self._execCommand(args, shell=False)
        print "Done"

    def _genCode(self):
        print "Generating code ..."
        self._mkDirNoError(self._getPath('gen'))
        self._mkDirNoError(self._getPath('bin'))
        self._mkDirNoError(self._getPath('bin', 'res'))
        self._mkDirNoError(self._getPath('res'))

        args = [
            os.path.join(self._buildToolsPath, 'aapt'),
            'package', '-f', '-m', '-0', 'apk', '-M',
            self._getPath('AndroidManifest.xml'),
            '--auto-add-overlay',
            '-I',
            self._getAndroidJar(),
            '-J',
            self._getPath('gen'),
            '-S',
            self._getPath('res'),
            '-S',
            self._getPath('bin', 'res')
        ]
        self._execCommand(args, shell=False)
        print "Done"

    def _crunch(self):
        print "Crunching ..."
        args = [
            os.path.join(self._buildToolsPath, 'aapt'),
            'crunch', '-v',
            '-S',
            self._getPath('res'),
            '-C',
            self._getPath('bin', 'res')
        ]
        self._execCommand(args, shell=False)
        print "Done"

    def _doCompile(self):
        self._genCode()
        self._compileJavaCode()
        self._dex()
        self._crunch()
        # crunch?

    def _packageResources(self):
        args = [
            os.path.join(self._buildToolsPath, 'aapt'),
            'package', '--no-crunch', '-f', '--debug-mode', '-0', 'apk', '-M',
            self._getPath('AndroidManifest.xml'),
            '-S',
            self._getPath('bin', 'res'),
            '-S',
            self._getPath('res'),
            '-I',
            self._getAndroidJar(),
            '-F',
            self._getPath('bin', self._getProjectName() + '.ap_'),
            '--generate-dependencies'
        ]
        self._execCommand(args, shell=False)

    def _doPackage(self):
        # Package resources
        self._packageResources()

        # Build the apk and sign it.
        args = [
            'java',
            "-classpath",
            os.path.join(self._androidHome, "tools", "lib", "sdklib.jar"),
            "com.android.sdklib.build.ApkBuilderMain",
            self._getPath('bin', self._getProjectName() + '-debug-unaligned.apk'),
            "-f",
            self._getPath('bin', 'classes.dex'),
            "-v",
            "-z",
            self._getPath('bin', self._getProjectName() + '.ap_')
        ]
        self._execCommand(args, shell=False)

        # Zip-align.
        args = [
            os.path.join(self._buildToolsPath, 'zipalign'),
            '-f',
            '4',
            self._getPath('bin', self._getProjectName() + '-debug-unaligned.apk'),
            self._getPath('bin', self._getProjectName() + '-debug.apk'),
        ]
        self._execCommand(args, shell=False)
        print "APK file is in: %s" % (self._getPath('bin', self._getProjectName() + '-debug.apk'))

    def _doLaunch(self):
        print 'Installing ...'
        # Should idealy handle multiple devices. This would install it on the first one.
        args = [
            os.path.join(self._androidHome, 'platform-tools', 'adb'),
            'install',
            '-r',
            self._getPath('bin', self._getProjectName() + '-debug.apk')
        ]
        self._execCommand(args, shell=False)
        print 'Done'

    def _doClean(self):
        print 'Cleaning ...'
        # Should idealy handle multiple devices. This would install it on the first one.
        args = [
            'rm',
            '-rf',
            self._getPath('bin'),
            self._getPath('gen')
        ]
        print "Cleaning: %s" % (args)

        # Deliberately disabled to prevent potential harm. Enable if needed.
        #self._execCommand(args, shell=False)
        print 'Done'

    def compileProject(self):
        self._loadConfig()
        self._runCmd(CMP_BEFORE)
        self._doCompile()
        self._runCmd(CMP_AFTER)

    def packageProject(self):
        self._loadConfig()
        self._runCmd(PKG_BEFORE)
        self._doPackage()
        self._runCmd(PKG_AFTER)

    def launchProject(self):
        self._loadConfig()
        self._runCmd(LNCH_BEFORE)
        self._doLaunch()
        self._runCmd(LNCH_AFTER)

    def cleanProject(self):
        self._loadConfig()
        self._runCmd(CLEAN_BEFORE)
        self._doClean()
        self._runCmd(CLEAN_AFTER)


def createProject(args):
    ub = UberBuild(args)
    ub.createProject()

def compileProject(args):
    ub = UberBuild(args)
    ub.compileProject()

def packageProject(args):
    ub = UberBuild(args)
    ub.packageProject()

def launchProject(args):
    ub = UberBuild(args)
    ub.launchProject()

def cleanProject(args):
    ub = UberBuild(args)
    ub.cleanProject()

class UberBuildArgumentsParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write('error: %s\n' % message)
        self.print_help()
        sys.exit(2)


parser = UberBuildArgumentsParser(description='Android Build System.')

subParsers = parser.add_subparsers(help='Commands')

# Create project command.
parserCreateProject = subParsers.add_parser('create_project', help='Creates a new Android application project.')
parserCreateProject.add_argument('path', nargs='?', help='Project location', default='newProject')
parserCreateProject.add_argument('--name', help='Project name (e.g. UberApp)', default='UberApp')
parserCreateProject.add_argument('--package', help='Package name (e.g. com.uber.myapp)', default='com.uber.myapp')
parserCreateProject.add_argument('--target', help='Android target api (e.g. android-23)')
parserCreateProject.add_argument('--activity', help='Project location', default='MainActivity')
parserCreateProject.set_defaults(func=createProject)

# Compile project command.
parserCompileProject = subParsers.add_parser('compile', help='Compiles an existing Android project.')
parserCompileProject.add_argument('path', nargs='?', help='Project location', default=".")
parserCompileProject.set_defaults(func=compileProject)

# Package project command.
parserPackageProject = subParsers.add_parser('package', help='Packages an existing Android project.')
parserPackageProject.add_argument('path', nargs='?', help='Project location', default=".")
parserPackageProject.set_defaults(func=packageProject)

# Launch project command.
parserLaunchProject = subParsers.add_parser('launch', help='Launches an existing Android project.')
parserLaunchProject.add_argument('path', nargs='?', help='Project location', default=".")
parserLaunchProject.set_defaults(func=launchProject)

# Clean project command.
parserCleanProject = subParsers.add_parser('clean', help='Cleans existing Android project.')
parserCleanProject.add_argument('path', nargs='?', help='Project location', default=".")
parserCleanProject.set_defaults(func=cleanProject)

args = parser.parse_args()
#print "Args: [%s]" % (args)
args.func(args)

